package ss.week3.bill;

public class Bill {

    public Printer printer;
    private double totalSum;

    /**
     * Take a String text and a double price; its toString method should return the text, and its getPrice method should return the price.
     */
    public interface Item {

        // Returns the price of this item
        double getPrice();
    }

    /**
     * Constructs a Bill sending the output to a given Printer.
     * @param printer the printer to send the bill to
     */
    public Bill(Printer printer) {
        this.printer = printer;
    }

    /**
     * Adds an item to this Bill. The item is sent to the printer, and the price is added to the sum of the Bill
     * @param item - item
     */
    public void addItem(Bill.Item item) {
        this.printer.printLine(item.toString(),item.getPrice());
        totalSum += item.getPrice();
    }

    /**
     * Sends the sum total of the bill to the printer.
     */
    public void close() {
        this.printer.printLine("TOTAL:", this.getSum());
    }

    /**
     * Calculate the total sum of the bill
     * @return returns the current sum total of the Bill.
     */
    public double getSum() {
        return totalSum;
    }
}
