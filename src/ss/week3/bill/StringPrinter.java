package ss.week3.bill;

public class StringPrinter implements Printer {

    String fullString = null;

    @Override
    public void printLine(String text, double price) {
        fullString += format(text, price);
    }

    public String getResult() {
        return fullString;
    }

//    public static void main(String[] args) {
//        StringPrinter p = new StringPrinter();
//        p.printLine("Appel", 24.0);
//        p.printLine("Taarten van Abel", 24.89457);
//
//        System.out.println(p.getResult());
//    }
}


