package ss.week3.bill;

public interface Printer {

    /**
     * Return Bill
     * @param text - item
     * @param price - price
     * @return a formatted line listing the item and price, ending on a newline
     */
    default String format(String text, double price) {
        return String.format("%-20s %1.2f", text, price);
    }

    /**
     * Uses format(). Send the combination of text and price to the printer.
     * @param text - item
     * @param price - price
     */
    default void printLine(String text, double price) {
        System.out.println(format(text, price));
    };

}
