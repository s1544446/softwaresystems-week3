package ss.week3.bill;

public class SysoutPrinter implements Printer {

    public static void main(String[] args) {
        SysoutPrinter p = new SysoutPrinter();
        p.printLine("Appel", 24.0);
        p.printLine("Taarten van Abel", 24.89457);
    }
}
