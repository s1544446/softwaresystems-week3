package ss.week3.password;

public class BasicPassword {

    public static final String INITIAL = "initial";
    private String password;

    public BasicPassword() {
        password = INITIAL;
    }

    /**
     * Test if a given string is an acceptable password.
     * Not acceptable: A word with less than 6 character or a word that contains a space.
     * @param suggestion - Word that should be tested
     * @return true If suggestion is acceptable
     */
    public boolean acceptable(String suggestion) {
        return (suggestion.length() >= 6 && !suggestion.contains(" "))? true: false;
    }

    /**
     * Changes this password.
     * @param oldpass - The current password
     * @param newpass - The new password
     * @return true If oldPass is equal to the current password and newpass is an acceptable password
     */
    public boolean setWord(String oldpass, String newpass) {
        if(this.testWord(oldpass) && this.acceptable(newpass)) {
            password = newpass;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Tests if a given word is equal to the current password.
     * @param test - Word that should be tested
     * @return true If test is equal to the current password
     */
    public boolean testWord(String test) {
        return password.equals(test);
    }

}
