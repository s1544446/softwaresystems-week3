package ss.week3.password;

public class Password {

    public String passwordText;
    public Checker checker;

    //Receives a Checker as parameter and sets checker and initPass to an appropriate value
    public Password(Checker checker) {
        this.checker = checker;
        passwordText = checker.generatePassword();
    }

    //It sets the checker by creating a BasicChecker object
    // and calling the first constructor with this object as parameter.
    public Password() {
        this(new BasicChecker());
    }

    /**
     * Test if a given string is an acceptable password.
     * Not acceptable: A word with less than 6 character or a word that contains a space.
     * @param suggestion - Word that should be tested
     * @return true If suggestion is acceptable
     */
    public boolean acceptable(String suggestion) {
        return this.checker.acceptable(suggestion);
    }

    /**
     * Changes this password.
     * @param oldpass - The current password
     * @param newpass - The new password
     * @return true If oldPass is equal to the current password and newpass is an acceptable password
     */
    public boolean setWord(String oldpass, String newpass) {
        if(this.testWord(oldpass) && this.acceptable(newpass)) {
            passwordText = newpass;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Tests if a given word is equal to the current password.
     * @param test - Word that should be tested
     * @return true If test is equal to the current password
     */
    public boolean testWord(String test) {
        return passwordText.equals(test);
    }

    public String getInitPass() {
        return passwordText;
    }

    public Checker getChecker() {
        return checker;
    }
}
