package ss.week3.password;

//Checks if the String is at least 6 characters long and does not contain any spaces,
public class BasicChecker implements Checker {

    @Override
    public boolean acceptable(String suggestion) {
        return suggestion.length() >= 6 && !suggestion.contains(" ")? true: false;
    }

    @Override
    public String generatePassword() {
        return "ThisIsMyCoolPassword218";
    }
}
