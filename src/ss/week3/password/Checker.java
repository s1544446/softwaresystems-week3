package ss.week3.password;

public interface Checker {

    //should return true if the parameter is an acceptable String.

    /**
     * Check if the suggestion is an acceptable password.
     * @param suggestion - potential password
     * @return true if the suggestion is acceptable
     */
    boolean acceptable(String suggestion);


    /**
     * Generate an acceptable password.
     * @return an acceptable String.
     */
    String generatePassword();

}
