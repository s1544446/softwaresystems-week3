package ss.week3.password;

import java.lang.Character;

public class StrongChecker extends BasicChecker {

    @Override
    public boolean acceptable(String suggestion) {
        boolean firstLetter = Character.isLetter(suggestion.charAt(0));
        boolean lastDigit = Character.isDigit(suggestion.charAt(suggestion.length()));

        return super.acceptable(suggestion) && firstLetter && lastDigit? true: false;

    }

    @Override
    public String generatePassword() {
        return "ThisIsMyCoolPassword218";
    }

}
