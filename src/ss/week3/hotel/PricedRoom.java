package ss.week3.hotel;

import ss.week3.hotel.Safe;
import ss.week3.bill.Bill;

public class PricedRoom extends Room implements Bill.Item {

    private int roomNumber;
    private double roomPrice;
    private double safePrice;

    PricedSafe pricedSafe;

    public PricedRoom(int roomNumber, double roomPrice, double safePrice) {
        super(roomNumber, new PricedSafe(safePrice));
        this.roomNumber = roomNumber;
        this.roomPrice = roomPrice;
        this.safePrice = safePrice;

    }

    @Override
    public double getPrice() {
        return roomPrice;
    }

    @Override
    public String toString() {
        return "PricedRoom{" +
                "roomNumber=" + this.roomNumber +
                ", roomPrice=" + this.roomPrice +
                '}';
    }
}
