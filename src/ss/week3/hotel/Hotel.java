package ss.week3.hotel;

import ss.src.ss.week2.hotel.Guest;
import ss.week3.bill.Bill;
import ss.week3.bill.Printer;
import ss.week3.bill.StringPrinter;
import ss.week3.hotel.Room;

public class Hotel {

    public String name;
    private PricedRoom k101;
    private Room k102;
    private Room[] rooms;
    public static final double ROOM_PRICE = 20;
    public static final double SAFE_PRICE = 10.50;


    public Hotel(String name) {
        this.name = name;
        k101 = new PricedRoom(101, ROOM_PRICE, SAFE_PRICE );
        k102 = new PricedRoom(102, ROOM_PRICE, SAFE_PRICE );
        rooms = new Room[] {k101, k102};
    }

    /**
     * The method assigns a room to a guest. Return null if the hotel is full or if there is already a guest with the name.
     * @param guestName
     * @return a Room object with a (new) Guest of the given name checked in, or null in case there is already a guest with this name or the hotel is full.
     */
    public Room checkIn(String guestName) {

        Room freeRoom = getFreeRoom();
        Room roomWithGuest = getRoom(guestName);

        if (freeRoom != null && roomWithGuest == null) {
            Guest guest1 = new Guest(guestName);
            freeRoom.setGuest(guest1);
            System.out.printf("Guest %s gets room %s%n", guestName, freeRoom.getNumber());
            return freeRoom;
        }

        else {
            System.out.printf("Checkin failed: rooms are taken.%n");
            return null;
        }
        }

    /**
     * The guest is checked out, and the safe in the room is deactivated. Nothing happens if there is no guest with this name.
     * @param guestName
     */
    public void checkOut(String guestName) {
        Room guestRoom = getRoom(guestName);

        if(guestRoom != null) {
            guestRoom.setGuest(null);
            guestRoom.getSafe().deactivate();
            System.out.printf("Guest %s successfully checked out.%n", guestName);
        }

        else {
            System.out.printf("Checkout failed: room is empty or is not taken by %s.%n", guestName);
        }
    }

    /**
     * @return the Room into which the guest can check in, or null if there is no free room available.
     */
    public Room getFreeRoom() {
        Room freeRoom = null;

        for (int i = 0; i < rooms.length; i++) {

            if (rooms[i].getGuest() == null) {
                freeRoom = rooms[i];
            }
        }
        return freeRoom;
    }

    /**
     * @param guestName
     * @return the Room object into which the guest has checked in, or null if the guest cannot be found in any room.
     */
    public Room getRoom(String guestName) {

        Room roomWithGuest = null;

        for (int i = 0; i < this.rooms.length; i++) {

            if (rooms[i].getGuest() != null && rooms[i].getGuest().getName().equals(guestName)) {
                roomWithGuest = rooms[i];
            }
        }
        return roomWithGuest;
    }

    /**
     * @return a textual description of all rooms in the hotel, including the name of the guest and the status of the safe in that room.
     */
    public String toString() {

        String guestNameOne = "empty";
        String guestNameTwo = "empty";

        if (k101.getGuest() != null) {
            guestNameOne = k101.getGuest().getName();
        }

        if (k102.getGuest() != null) {
            guestNameTwo = k102.getGuest().getName();
        }

        return "In the " + this.name + " room 101 has " + guestNameOne +" and room 102 has " + guestNameTwo;
    }

    /**
     * Print out the bill
     * @param guestName - name of the guest
     * @param nightsStayed - the number of nights the guest spent in the hotel
     * @param printer - printer to print the bill
     * @return the bill. If there is no guest with the given name, or if the guest stays in an ’standard’ room (i.e., not a PricedRoom),
     * the method getBill should return the value null. The bill should also include the use of the safe!
     */
    public Bill getBill(String guestName, int nightsStayed, Printer printer) {

        //Check preconditions
        Room roomWithGuest = getRoom(guestName);

        //Create new bill
        Bill finalBill = new Bill(printer);

        if(roomWithGuest != null && roomWithGuest instanceof PricedRoom) {
            //Add item to bill for each night
            for (int i = 0; i < nightsStayed; i++) {
                Item night = new Item("night " + i, ROOM_PRICE);
                finalBill.addItem(night);
            }

            //If safe gets activated, add item to bill for safe
            if(getRoom(guestName).getSafe().isActive()){
                Item safe = new Item("safe ", SAFE_PRICE);
                finalBill.addItem(safe);
            }

            finalBill.close();
        }

        return finalBill;
    }

    public class Item implements Bill.Item {
        double price;
        String text;

        public Item(String text, double price) {
            this.text = text;
            this.price = price;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return text;
        }

    }
}
