package ss.week3.hotel;

import ss.week3.bill.Bill;
import ss.week3.password.Password;

public class PricedSafe extends Safe implements Bill.Item {

    double safePrice;
    Password safePassword;

    public PricedSafe(double price){
        this.safePrice = price;
        safePassword = new Password();
    }

    /**
     * Activates the safe if the password is correct
     * @param passwordText - password
     */
    public void activate(String passwordText) {
        assert passwordText != null;

        if (this.safePassword.getInitPass().equals(passwordText)) {
            super.activate();
        }

        else {
            System.out.println("Wrong password mate!");
            super.deactivate();
        }

        this.close();
    }

    /**
     * Gives a warning and does not activate the safe
     */
    @Override
    public void activate() {
        System.out.println("Please insert a password. Safe is not activated!");
    }

    /**
     * Opens the safe if it is active, and the password is correct;
     * @param passwordText - password
     */
    public void open(String passwordText) {
        assert passwordText != null;

        if (this.safePassword.passwordText.equals(passwordText) && this.isActive()) {
            super.open();
        }
    }

    /**
     * Does not change the state of the safe
     */
    @Override
    public void open() {

    }

    /**
     * @return the password object on which the method testWord can be called to check the password.
     */
    public Password getPassword(){
        return safePassword;
    }

    @Override
    public double getPrice() {
        return safePrice;
    }

    /**
     * include the price of the safe
     * @return
     */
    public String toString() {
        String priceText = Double.toString(this.safePrice);
        return "PricedSafe: " + priceText;
    }

    public static void main(String[] args) {
        PricedSafe testSafe = new PricedSafe(25);
        testSafe.open(null);
    }


}
