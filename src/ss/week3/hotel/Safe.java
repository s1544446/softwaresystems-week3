package ss.week3.hotel;

public class Safe {
    private boolean isOpened;
    private boolean isActivated;

    public Safe() {
        this.isOpened = false;
        this.isActivated = false;
    }

    /**
     * Activates the safe
     */
    public void activate() {
        this.isActivated = true;
        System.out.println("The (Priced)Safe is activated");
    }

    /**
     * Closes the safe and deactivates it
     */
    public void deactivate() {
        this.isActivated = false;
        this.isOpened = false;
        System.out.println("The (Priced)Safe is deactivated");
    }

    /**
     * opens the safe if it is active
     */
    public void open() {
        isOpened = this.isActivated;
    }

    /**
     * Closes the safe (but does not change its active/ inactive status)
     */
    public void close() {
        isOpened = false;
    }

    /**
     * @return true if the safe is active, false otherwise.
     */
    public boolean isActive() {
        return isActivated;
    }

    /**
     * @return true if the safe is open, false otherwise
     */
    public boolean isOpen() {
        return isOpened;
    }
}
