package ss.week3.test;
import org.junit.jupiter.api.*;
import ss.week3.bill.Bill;
import ss.week3.bill.StringPrinter;
import static org.junit.jupiter.api.Assertions.*;

public class BillTest {

    private Bill bill;
    private StringPrinter stringPrinter;

    @BeforeEach
    public void setUp() {
        this.stringPrinter = new StringPrinter();
        this.bill = new Bill(this.stringPrinter);
    }

    /**
     * Testing the begin state of a Bill (Contains no items)
     */
    @Test
    public void testBeginState() {
        assertEquals(0.00, bill.getSum());
    }

    /**
     * Testing whether items are inserted correctly and the bill can properly be closed.
     */
    @Test
    public void testNewItem() {

        //Create appel and add to bill
        Item appel = new Item("Appel", 5.94);
        bill.addItem(appel);

        //Check if appel and price is added
        this.stringPrinter.getResult().contains("Appel");
        this.stringPrinter.getResult().contains("5.94");


        //Create bike and add to bill
        Item bike = new Item("Bike", 25.9);
        bill.addItem(bike);

        //Check if bike + price is added
        this.stringPrinter.getResult().contains("Bike");
        this.stringPrinter.getResult().contains("25.9");

        bill.close();
        this.stringPrinter.getResult().contains("TOTAL:");
        this.stringPrinter.getResult().contains("31,84");

    }

    public class Item implements Bill.Item {
        double price;
        String text;

        public Item(String text, double price) {
            this.text = text;
            this.price = price;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return text;
        }

    }

}
