package ss.week3.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ss.week3.bill.Bill;
import ss.week3.hotel.PricedSafe;

public class PricedSafeTest {

    private PricedSafe safe;
    private static final double PRICE = 6.36;
    private static final String PRICE_PATTERN = ".*6[.,]36.*";

    public String CORRECT_PASSWORD;
    public String WRONG_PASSWORD;


    @BeforeEach
    public void setUp() throws Exception {
        safe = new PricedSafe(PRICE);
        CORRECT_PASSWORD = safe.getPassword().getInitPass();
        WRONG_PASSWORD = CORRECT_PASSWORD + "WRONG";
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());
    }

    @Test
    public void testIsBillItem() throws Exception {
    	assertTrue(safe instanceof Bill.Item,
    			"safe should be an instance of Bill.Item.");
        assertEquals(PRICE, safe.getPrice(), 0,
        		"GetPrice should return the price of the safe.");
    }

    @Test
    public void testGetPrice() {
        assertEquals(6.36, safe.getPrice());
    }

    @Test
    public void testToString() {
        assertEquals("PricedSafe: 6.36", safe.toString());
    }

    @Test
    public void testActivate() {
        //Test of safe is deactivated at default

        assertFalse(safe.isActive());

        //Test if safe gets activated with right password
        safe.activate(CORRECT_PASSWORD);
        assertTrue(safe.isActive());
        assertFalse(safe.isOpen());

        //Test if safe is deactivated with wrong password
        safe.activate(WRONG_PASSWORD);
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());

        //Test if safe is deactivated with no password
        safe.activate();
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());
    }

    @Test
    public void testOpen() {

        //Test if a deactivated safe remains deactivated and closed when trying to open with correct password
        safe.open(CORRECT_PASSWORD);
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());

        //Test if a deactivated safe remains deactivated and closed when trying to open with wrong password
        safe.open(WRONG_PASSWORD);
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());

        //Test that after activating a safe with the correct password it cannot be opened with an incorrect
        //password, but after being opened with the correct password it is activated and open;
        safe.activate(CORRECT_PASSWORD);
        safe.open(CORRECT_PASSWORD);
        assertTrue(safe.isActive());
        assertTrue(safe.isOpen());

        safe.activate(CORRECT_PASSWORD);
        safe.open(WRONG_PASSWORD);
        assertTrue(safe.isActive());
        assertFalse(safe.isOpen());
    }

    @Test
    public void testClose() {
        //Test if after activating and opening a safe with the correct password, and closing it, the safe is
        //closed and activated;
        safe.activate(CORRECT_PASSWORD);
        safe.open(CORRECT_PASSWORD);
        safe.close();
        assertTrue(safe.isActive());
        assertFalse(safe.isOpen());

        //Test if after closing a deactivated safe, it is closed and deactivated.
        safe.deactivate();
        safe.close();
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());

    }

}
